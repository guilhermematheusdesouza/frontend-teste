import formValidation from './js/formValidation';
import fieldMask from './js/fieldMask';
import './scss/app.scss';

fieldMask();
formValidation();
