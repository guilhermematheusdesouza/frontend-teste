import mask from 'jquery.maskedinput/src/jquery.maskedinput';

export default () => {
	$('.cnpj').mask('99.999.999/9999-99');
	$('.telefone').mask('(99) 9999-9999?9', { placeholder: '_' }).change(event => {
		let target, phone, element;
		target = (event.currentTarget) ? event.currentTarget : event.srcElement;
		phone = typeof target != 'undefined' ? target.value.replace(/\D/g, '') : '';
		element = $(target);
		element.unmask();

		if (phone.length > 10) {
			element.mask('(99) 99999-999?9', {
				placeholder: '_'
			});
		} else {
			element.mask('(99) 9999-9999?9', {
				placeholder: '_'
			});
		}
	});
}
