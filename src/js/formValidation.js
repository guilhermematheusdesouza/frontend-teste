const replaceValidationUI = form => {
	form.addEventListener('invalid', event => {
		event.preventDefault();
	}, true);

	form.addEventListener('submit', event => {
		if (!this.checkValidity()) {
			event.preventDefault();
		}
	});

	let btnSubmit = form.querySelector('button:not([type=button]), input[type=submit]');
	btnSubmit.addEventListener('click', event => {
		let invalidFields = form.querySelectorAll(':invalid'),
			errorMessages = form.querySelectorAll('.error-message'),
			parent;

		for (let i = 0; i < errorMessages.length; i++) {
			errorMessages[i].parentNode.removeChild(errorMessages[i]);
		}

		for (let i = 0; i < invalidFields.length; i++) {
			parent = invalidFields[i].parentNode;
			parent.insertAdjacentHTML('beforeend', `<div class="error-message text-light">${invalidFields[i].validationMessage}</div>`);
		}

		if (invalidFields.length > 0) {
			invalidFields[0].focus();
		}
	});
};

export default () => {
	const forms = document.querySelectorAll('form');
	for (let i = 0; i < forms.length; i++) {
		replaceValidationUI(forms[i]);
	}
}
