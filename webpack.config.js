const webpack = require("webpack");
const path = require('path');

module.exports = {
	context: path.resolve(__dirname, "src"),
	entry: {
		app: "./app.js",
	},
	output: {
		filename: "[name].bundle.js",
		path: path.resolve(__dirname, "public/build"),
		publicPath: "/build",
	},
	devServer: {
		contentBase: path.resolve(__dirname, "src"),
	},
	module: {
		rules: [{
			test: /\.js$/,
			exclude: path.resolve(__dirname, "node_modules"),
			loader: "babel-loader",
			options: {
				presets: ["es2015"]
			},
		},{
			test: /\.(scss)$/,
			use: [{
				loader: 'style-loader',
			}, {
				loader: 'css-loader',
			}, {
				loader: 'postcss-loader',
				options: {
					plugins: function () {
						return [
							require('precss'),
							require('autoprefixer')
						];
					}
				}
			}, {
				loader: 'sass-loader'
			}]
		}],
	},
	devServer: {
	  contentBase: path.join(__dirname, "public"),
	  compress: true,
	  port: 9000
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			Popper: ['popper.js', 'default']
		})
	]
};
